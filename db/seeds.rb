# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

# create 10 random Tag
10.times{ Tag.create(name: Faker::Lorem.word) }

# create 9 random User
9.times{ user = User.create(name: Faker::Name.name, email: Faker::Internet.email) }

# create 5 random Author
5.times{ author =  Author.create(name: Faker::Name.name, email: Faker::Internet.email, 
	bio: Faker::Lorem.paragraph, password: Faker::Internet.password) }

	# create up to twelve Entry published by a random Author
	(rand(12)+2).times{
		entry = Entry.new(title: Faker::Name.title, content: Faker::Lorem.paragraph, publish_date: Faker::Business.credit_card_expiry_date)
		entry.author = Author.find(rand(5)+1)
		entry.save

		# add up to four Tag to the new Entry
		(rand(4)+2).times{ 
			n = rand(9)+1
			entry.tags << Tag.find(n) if entry.tags.exists?(n) == false 
		}


		# add up to two random Comment published by a random Author to the new Entry
		(rand(2)+1).times{
			comment = Comment.new(content: Faker::Lorem.sentence, publish_date: Faker::Business.credit_card_expiry_date)
			comment.commenter = Author.find(rand(5)+1)
			comment.entry = entry
			comment.save
		}

		# add up to three random Comment published by a random User to the new Entry
		(rand(3)+1).times{
			comment2 = Comment.new(content: Faker::Lorem.sentence, publish_date: Faker::Business.credit_card_expiry_date)
			comment2.commenter = User.find(rand(9)+1)
			comment2.entry = entry
			comment2.save
		}
	}


