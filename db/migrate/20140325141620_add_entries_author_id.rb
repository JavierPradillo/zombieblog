class AddEntriesAuthorId < ActiveRecord::Migration
  def change
    change_table :entries do |t|
      t.references :author
    end

    add_index :entries, :author_id
  end
end
