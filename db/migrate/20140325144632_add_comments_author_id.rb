class AddCommentsAuthorId < ActiveRecord::Migration
  def change
    change_table :comments do |t|
      t.references :commenter
      t.string :commenter_type
    end

    add_index :comments, :commenter_id
  end
end
