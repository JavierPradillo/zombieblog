class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :content
      t.date :publish_date
      t.references :entry
      t.boolean :approved, default: true

      t.timestamps
    end
    add_index :comments, :entry_id
  end
end
