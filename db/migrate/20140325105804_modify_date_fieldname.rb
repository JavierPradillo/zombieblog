class ModifyDateFieldname < ActiveRecord::Migration
  def change 
  	rename_column :entries, :date, :publish_date
  end
end
