class AuthorsController < ApplicationController

  def show
    @author = Author.find(params[:id])
    @entries = @author.entries.published.by_date.all
  end

end
