class TagsController < ApplicationController

def show
    @tag = Tag.find(params[:id])
    @entries = @tag.entries.published.by_date.all
end

end
