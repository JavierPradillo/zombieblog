class ApplicationController < ActionController::Base
	
	protect_from_forgery
	before_filter :tag_cloud, :login
	before_filter :set_locale
	
	# app/controllers/application_controller.rb
	def default_url_options(options={})
	  logger.debug "default_url_options is passed options: #{options.inspect}\n"
	  { locale: I18n.locale }
	end						

	def set_locale
		I18n.locale = params[:locale] || I18n.default_locale
	end

	protected
	def tag_cloud
	@tags = Tag.all.sort_by{|e| e.article_count}.reverse!
	end

	def login
	@c_author = current_author
	end

end