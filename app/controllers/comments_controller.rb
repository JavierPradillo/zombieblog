class CommentsController < ApplicationController

  def create
    @entry = Entry.find(params[:entry_id])
    @comment = @entry.comments.new(params[:comment])
    
    unless @c_author.nil?
      @comment.email = @c_author.email
      @comment.name = @c_author.name
    end

    if @comment.save
      redirect_to @entry, notice: 'Comment succesfully published'
    else
      redirect_to @entry, notice: "Error: comment couldn't be published"
    end
  end

  def inappropiate
    @entry = Entry.find(params[:entry_id])
    @comment = Comment.find(params[:id])
    @comment.mark_as_spam
    redirect_to  @entry, notice: 'Comment marked as inappropiate'
  end

end
