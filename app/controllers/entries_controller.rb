class EntriesController < ApplicationController
  before_filter :authenticate_author!, except: [ :index, :show]
  before_filter :get_entry, only: [ :edit, :update, :show]
  before_filter :authorize, only: [ :edit, :update]

  def index
    @entries = Entry.published.by_date.all	
  end

  def show
    @comments =  @entry.comments.approving.by_date.all
  end

  def new
    @entry = Entry.new  
  end

  def edit
  end

  def create

    @entry = current_author.entries.new(params[:entry])

    if @entry.save
      redirect_to @entry, notice: 'Entry succesfully published'
    else
      redirect_to @entry, notice: "Error: entry couldn't be published"
    end
  end

  def update

    if @entry.update_attributes(params[:entry])
      redirect_to @entry, notice: 'Entry succesfully updated'
    else
      render action: "edit", notice: "Error: entry couldn't be updated"
    end

  end

  def destroy
    @entry = Entry.find(params[:id])
    @entry.destroy

    redirect_to @c_author, notice: 'Entry deleted'
  end

  private
  def authorize
    if @entry.author != current_author
      redirect_to @entry, notice: 'You are not allowed to edit other author entries'
    end
  end

  def get_entry
    @entry = Entry.find(params[:id])
  end
  
end