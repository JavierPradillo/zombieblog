class Entry < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  attr_accessible :content, :publish_date, :title
  
  validates_presence_of :content, :publish_date, :title, :author_id
  validates_length_of :content, minimum: 10

  has_and_belongs_to_many :tags
  has_many :comments, dependent: :destroy

  belongs_to :author

  scope :published, where("publish_date <= ? ", DateTime.now)
  scope :by_date, order("publish_date DESC")


  def should_generate_new_friendly_id?
    new_record? || slug.blank?
  end

end
