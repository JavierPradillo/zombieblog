class User < ActiveRecord::Base
  attr_accessible :email, :name, :commenter

  validates_presence_of :name, :email
  validates_uniqueness_of :name, :email
  validates_email_format_of :email 

  has_many :comments, :as => :commenter
end
