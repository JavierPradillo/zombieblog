class Author < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  
  attr_accessible :bio, :name, :photo

  validates_presence_of :name, :email
  validates_uniqueness_of :name, :email
  validates_email_format_of :email 

  has_many :entries
  has_many :comments, :as => :commenter

end
