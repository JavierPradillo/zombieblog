class Comment < ActiveRecord::Base
  attr_accessible :content, :publish_date, :approved, :name, :email, :commenter

  validates_presence_of :content, :publish_date, :entry_id
  validates_length_of :content, minimum: 10

  belongs_to :entry
  belongs_to :commenter, :polymorphic => true

  scope :approving, where(approved: true)
  scope :by_date, order("publish_date DESC")

  attr_accessor :name, :email

  before_validation :set_commenter, :unless => :commenter
  before_validation :set_date

  def mark_as_spam
    self.update_attribute(:approved, false)
  end

  private

  def set_commenter
    commenter_instance = Author.where(email: email).first
    if commenter_instance.nil?
       commenter_instance = User.where(:email => email).first
    end
    if commenter_instance.nil?
      commenter_instance = User.new(name: name, email: email)
      commenter_instance.save
    end
    self.commenter = commenter_instance
  end

  def set_date
    self.publish_date = DateTime.now
  end
end
